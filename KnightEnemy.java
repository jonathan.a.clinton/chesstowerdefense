package ChessTowerDefense;

import java.awt.Point;
import java.util.ArrayList;

public class KnightEnemy extends Enemy {
	KnightEnemy(int level){
		health = 100 * level;
		damage = 1*level;
		speed = 1*level;
		xLoc = 1;
		yLoc = 0;
		alive = true;
		image = Render.knight.i;
	}
	@Override
	public ArrayList<Point> getPossibleMoves() {
		// TODO Auto-generated method stub
		ArrayList<Point> moves = new ArrayList<Point>();
		Point temp = new Point(xLoc+1, yLoc+2);
		if(xLoc < 14 && yLoc < 13)
			moves.add(temp);
		if(!moves.isEmpty())
			return moves;
		else
			return null;
	}

}
