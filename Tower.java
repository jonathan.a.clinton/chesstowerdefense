package ChessTowerDefense;



public abstract class Tower extends MovableEntity {

	public enum towerType implements EntityType{
		PAWN, KNIGHT, BISHOP, ROOK, QUEEN
	}
	private towerType tower;
	
	public Tower(towerType t){
		entity = t;
		health = 100;
		damage = 0;
		speed = 0;
		xLoc = 0;
		yLoc = 0;
		alive = true;
	}
	/*private int health;
	private int damage;
	private int speed;
	//keeping track of which square on the board the enemy is in
	private int xLoc;
	private int yLoc;
	private boolean alive;
	BufferedImage image;
	
	public towerType getTower()
	{
		return tower;//how is returning an enum handled
	}
	public int getHealth()
	{
		return health;
	}

	public int getDamage()
	{
		return damage;
	}
	public int getSpeed()
	{
		return speed;
	}
	public int getX()
	{
		return xLoc;
	}
	public int getY()
	{
		return yLoc;
	}
	public boolean isAlive()
	{
		/
	
	/*
	 * Reduces the health by a given amount
	 * sets alive variable to false if health drops to 0 or below
	 */
	/*public void takeDamage(int damage)
	{
		health -= damage;
		if(health <= 0)
			alive = false;
	}
	*/
	/*
	 * Moves to the passed in location.
	 */
	/*public void move(int x, int y)
	{
		xLoc = x;
		yLoc = y;
	}*/

	
	/* 
	 * Moves a given distance.
	 */
	/*public void moveInDirection(int xAdjustment, int yAdjustment){
		xLoc += xAdjustment;
		yLoc += yAdjustment;
	}
	
	public BufferedImage getImage()
	{
		return image;
	}*/
	
}
