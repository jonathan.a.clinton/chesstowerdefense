package ChessTowerDefense;

import java.awt.*;

import javax.swing.*;

import java.awt.event.*;

public class ChessTowerDefense
{
	public static int mouseX = 0, mouseY = 0;
	public static boolean leftClick = false, rightClick = false;
	public static boolean wPressed = false, sPressed = false;
	public static boolean mousePressed = false;
	public static EnemyManager enemies = new EnemyManager();

	static JFrame frame = new JFrame("Chess Tower Defense");
	static Render panel = new Render();
	static MouseMotion motion = new MouseMotion();
	static MouseClick click = new MouseClick();
	static Keyboard keyboard = new Keyboard();
	
	public static void main(String[] args)
	{

/*
		JFrame frame = new JFrame("Chess Tower Defense");
		Render panel = new Render();
		MouseMotion motion = new MouseMotion();
		MouseClick click = new MouseClick();
		Keyboard keyboard = new Keyboard();*/
		frame.addKeyListener(keyboard);
		frame.addMouseMotionListener(motion);
		frame.addMouseListener(click);
		frame.add(panel);

		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();

		Shop.setUp();

		enemies.createWave(1);
		
		Thread thr = new Thread(new ThreadRunner());
		thr.start();
		
	}

	public static Point getMouse()
	{
		return new Point(mouseX, mouseY);
	}
}

