package ChessTowerDefense;
import java.awt.Graphics2D;
import java.awt.Point;
//import ChessTowerDefense.Enemy.enemyType;
import java.util.ArrayList;

public class EnemyManager {
	private ArrayList<Enemy> enemies = new ArrayList<Enemy>();
	
	
	public void createWave(int waveNumber){
		for(int i = 0; i < waveNumber; ++i){
			enemies.add(new PawnEnemy(1));
			enemies.add(new KnightEnemy(1));
		}
	}

	public void moveEnemies(){
		for(Enemy e : enemies){
			ArrayList<Point> moves = e.getPossibleMoves();
			if(moves != null)
				e.move(moves.get(0));
		}

	}
	public void drawEnemies(Graphics2D g){
		for(Enemy e : enemies){
			e.draw(g);
		}
	}
}
