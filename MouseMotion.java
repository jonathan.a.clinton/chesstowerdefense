package ChessTowerDefense;

import java.awt.event.*;
import java.awt.*;

public class MouseMotion extends MouseMotionAdapter
{
	public static int xBias = 9, yBias = 30;

	public void mouseClicked(MouseEvent e){
		ChessTowerDefense.mousePressed = true;
	}
	public void mouseMoved(MouseEvent e)
	{
		ChessTowerDefense.mouseX = e.getX() - xBias;
		ChessTowerDefense.mouseY = e.getY() - yBias;
	}

	public void mouseDragged(MouseEvent e)
	{
		ChessTowerDefense.mouseX = e.getX() - xBias;
		ChessTowerDefense.mouseY = e.getY() - yBias;
	}
}
