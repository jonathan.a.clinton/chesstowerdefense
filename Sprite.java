
package ChessTowerDefense;

import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import javax.imageio.ImageIO;
import java.io.*;
//Code from http://stackoverflow.com/questions/665406/how-to-make-a-color-transparent-in-a-bufferedimage-and-save-as-png

public class Sprite
{
	public BufferedImage i;

	public Sprite(BufferedImage source)
	{
        Image image = makeColorTransparent(source,new Color(255,255,255));
        BufferedImage i2 = imageToBufferedImage(image);
        Image image2 = makeColorTransparent(i2, new Color(254,254,254));
        i = imageToBufferedImage(image2);
	}

    public static BufferedImage imageToBufferedImage(Image image)
    {
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = bufferedImage.createGraphics();
        g2.drawImage(image, 0, 0, null);
        g2.dispose();

        return bufferedImage;
    }

    public static Image makeColorTransparent(BufferedImage im, final Color color)
    {
        ImageFilter filter = new RGBImageFilter()
        {
                // the color we are looking for... Alpha bits are set to opaque
                public int markerRGB = color.getRGB() | 0xFF000000;

                public final int filterRGB(int x, int y, int rgb)
                {
                        if ((rgb | 0xFF000000) == markerRGB)
                        {
                            // Mark the alpha bits as zero - transparent
                            return 0x00FFFFFF & rgb;
                        }
                        else
                        {
                            // nothing to do
                        	return rgb;
                        }
                }
        };

        ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }
}