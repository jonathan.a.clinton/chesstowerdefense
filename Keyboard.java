package ChessTowerDefense;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Keyboard extends KeyAdapter
{
	public void keyPressed(KeyEvent e)
	{
		char c = e.getKeyChar();
		if(c == 'w')
			ChessTowerDefense.wPressed = true;
		if(c == 's')
			ChessTowerDefense.sPressed = true;

	}

	public void keyReleased(KeyEvent e)
	{
		char c = e.getKeyChar();
		if(c == 'w')
			ChessTowerDefense.wPressed = false;
		if(c == 's')
			ChessTowerDefense.sPressed = false;
	}

}
