
package ChessTowerDefense;


import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class MouseClick extends MouseAdapter
{
	public void mousePressed(MouseEvent e)
	{
		if(e.getButton() == MouseEvent.BUTTON1)
	    	ChessTowerDefense.leftClick = true;
	    if(e.getButton() == MouseEvent.BUTTON3)
	    	ChessTowerDefense.rightClick = true;
	}

	public void mouseReleased(MouseEvent e)
	{
		if(e.getButton() == MouseEvent.BUTTON1)
	    	ChessTowerDefense.leftClick = false;
	    if(e.getButton() == MouseEvent.BUTTON3)
			ChessTowerDefense.rightClick = false;
	}


}
