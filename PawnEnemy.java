package ChessTowerDefense;

import java.awt.Point;
import java.util.ArrayList;

public class PawnEnemy extends Enemy {

	PawnEnemy(int level){
		health = 100 * level;
		damage = 1*level;
		speed = 1*level;
		xLoc = 0;
		yLoc = 0;
		alive = true;
		image = Render.pawn.i;
	}
	/*
	 * returns null if there are no possible moves
	 */
	@Override
	public ArrayList<Point> getPossibleMoves() {
		ArrayList<Point> moves = new ArrayList<Point>();
		Point temp = new Point(xLoc, yLoc+1);
		if(xLoc < 15 && yLoc < 14)
			moves.add(temp);
		if(!moves.isEmpty())
			return moves;
		else
			return null;
	}
}
