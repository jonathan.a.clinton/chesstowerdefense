package ChessTowerDefense;

import java.awt.Point;
import java.util.ArrayList;
import java.awt.Graphics2D;

public abstract class Enemy extends MovableEntity{
	
	/*public enum enemyType implements EntityType
	{
		PAWN, KNIGHT, BISHOP, ROOK, QUEEN
	};*/

	public Enemy(){
		//entity = e;
		health = 100;
		damage = 0;
		speed = 0;
		xLoc = 0;
		yLoc = 0;
		alive = true;
	}
	public abstract ArrayList<Point> getPossibleMoves();
	//private String name;
	/*private int health;
	private int damage;
	private int speed;
	//keeping track of which square on the board the enemy is in
	private int xLoc;
	private int yLoc;
	private boolean alive;
	BufferedImage image;
	
	public Enemy(){
		//name = "";
		health = 100;
		damage = 0;
		speed = 0;
		xLoc = 0;
		yLoc = 0;
		alive = true;
	}
	
	
	
	public String getName(){
		return name;
	}

	public int getHealth(){
		return health;
	}

	public int getDamage(){
		return damage;
	}
	public int getSpeed(){
		return speed;
	}
	public int getX(){
		return xLoc;
	}
	public int getY(){
		return yLoc;
	}
	public boolean isAlive(){
		return alive;
	}
	*/
	/*
	 * Reduces the health by a given amount
	 * sets alive variable to false if health drops to 0 or below
	 */
	/*public void takeDamage(int damage){
		health -= damage;
		if(health <= 0)
			alive = false;
	}*/
	public void draw(Graphics2D g) {
		// TODO Auto-generated method stub
		g.drawImage(image, xLoc * 40 ,yLoc * 40 ,null);
	}
	
	/*
	 * Moves to the passed in location.
	 */
	/*public void move(int x, int y){
		xLoc = x;
		yLoc = y;
	}*/
	
	/* 
	 * Moves a given distance.
	 */
	/*public void moveInDirection(int xAdjustment, int yAdjustment){
		xLoc += xAdjustment;
		yLoc += yAdjustment;
	}
	
	public BufferedImage getImage(){
		return image;
	}*/
	
}
