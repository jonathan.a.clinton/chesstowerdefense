package ChessTowerDefense;

import java.awt.image.BufferedImage;
import java.awt.Point;
import java.util.ArrayList;


public abstract class MovableEntity {
	/*public enum towerType{
		PAWN, KNIGHT, BISHOP, ROOK, QUEEN
	}*/
	//can we subclass and fill in the enumType with different things?
	interface EntityType{};
	protected EntityType entity;
	protected int health;
	protected int damage;
	protected int speed;
	//keeping track of which square on the board the enemy is in
	protected int xLoc;
	protected int yLoc;
	protected boolean alive;
	protected BufferedImage image;
	
	public EntityType getEntity()
	{
		return entity;//how is returning an enum handled
	}
	public int getHealth()
	{
		return health;
	}

	public int getDamage()
	{
		return damage;
	}
	public int getSpeed()
	{
		return speed;
	}
	public int getX()
	{
		return xLoc;
	}
	public int getY()
	{
		return yLoc;
	}
	public boolean isAlive()
	{
		return alive;
	}
	
	/*
	 * Reduces the health by a given amount
	 * sets alive variable to false if health drops to 0 or below
	 */
	public void takeDamage(int damage)
	{
		health -= damage;
		if(health <= 0)
			alive = false;
	}
	
	public abstract ArrayList<Point> getPossibleMoves();
	
	/*
	 * Moves to the passed in location.
	 */
	public void move(Point p)
	{
		xLoc = p.x;
		yLoc = p.y;
	}
	
	/* 
	 * Moves a given distance.
	 */
	public void moveInDirection(int xAdjustment, int yAdjustment){
		xLoc += xAdjustment;
		yLoc += yAdjustment;
	}
	
	public BufferedImage getImage()
	{
		return image;
	}
	
}
