package ChessTowerDefense;

import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.ImageIO;


public class Render extends JPanel
{
	public static int dimX = 800;
	public static int dimY = 600;
	public static Sprite pawn, knight;

	public Render()
	{
		super();
		this.requestFocus();
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(dimX,dimY));
		this.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		loadImages();
	}

	public void paint (Graphics r)
	{
		super.paint(r);
		Graphics2D g = (Graphics2D)r;
		g.setFont(new Font("dialog",0,15));
		g.drawString("Mouse X: " + ChessTowerDefense.mouseX,90,20);
		g.drawString("Mouse Y: " + ChessTowerDefense.mouseY,90,40);
		g.drawString("Left Click: " + ChessTowerDefense.leftClick,90,60);
		g.drawString("Right Click: " + ChessTowerDefense.rightClick,90,80);
		g.drawString("W Pressed: " + ChessTowerDefense.wPressed,90,100);
		g.drawString("S Pressed: " + ChessTowerDefense.sPressed,90,120);
		drawGrid(g);
		Shop.drawShop(g);

		ChessTowerDefense.enemies.drawEnemies(g);
		
		g.drawImage(pawn.i,40,240,null);
		g.drawImage(knight.i,80,280,null);
	}

	public static void loadImages()
	{
		try
		{
			pawn = new Sprite(ImageIO.read(new File("pawn.png")));
			knight = new Sprite(ImageIO.read(new File("knight.png")));
		}
		catch(Exception exc)
		{
			System.out.println("Picture not found!");
			System.exit(0);
		}


	}

	public void drawGrid(Graphics2D g)
	{
		for(int a = 1; a <= 14; ++a)
		{
			g.drawLine(a*40,0,a*40,dimY);
			g.drawLine(0,a*40,560,a*40);
		}


	}

	public static Point getCoordinates(Point p)
	{
		Point grid = new Point(-1,-1);
		if(p.getX() <= 560 && p.getX() >= 0 && p.getY() <= 600 && p.getY() >= 0)
			grid.setLocation((int)p.getX()/40,(int)p.getY()/40);
		return grid;
	}



}
