
package ChessTowerDefense;

import java.awt.*;
import java.awt.image.*;

public class Shop
{

	public static int money = 10;
	public static int[] costs = {1,3,1,1,1};
	public static BufferedImage units[] = {Render.pawn.i, Render.knight.i, Render.pawn.i, Render.pawn.i, Render.pawn.i};
	public static Rectangle squares[];
	public static int hover = -1, selected = -1; //hover: image mouse is over,  selected: unit selected to buy

	public static void setUp()
	{
		squares = new Rectangle[units.length];
		for(int a = 0; a < units.length; a++)
			squares[a] = new Rectangle(580,50 + a*50,40,40);
	}

	public static void drawShop(Graphics2D g)
	{
		g.drawLine(560,320,800,320);
		g.drawString("Money: " + money, 580,20);
		hover = -1;
		Point mouse = ChessTowerDefense.getMouse();
		for(int a = 0; a < units.length; a++)
		{
			g.drawImage(units[a],580,50 + a*50,null);
			g.drawString("" + costs[a],630,70 + a*50);
			if(squares[a].contains(mouse))
				hover = a;
		}

		if(hover >= 0 || selected >= 0)
		{
			int draw = selected;
			if(hover >= 0)
			{
				g.drawRect(575,45 + hover*50,50,50);
				draw = hover;
			}
			if(selected >= 0 && selected != hover)
				g.drawRect(575,45 + selected*50,50,50);
			if(draw == 0)
				g.drawString("Hi! I'm a pawn!",570,340);
			if(draw == 1)
				g.drawString("Hi! I'm a knight!",570,340);
		}

		if(selected >= 0)
		{
			mouse = Render.getCoordinates(mouse);
			if(mouse.getX() > -1)
			{
				g.drawImage(units[selected],(int)mouse.getX()*40,(int)mouse.getY()*40,null);

			}
		}

	}

	public static void checkClick(boolean left, boolean right)
	{
		Point p = ChessTowerDefense.getMouse();
		if(right)
			selected = -1;
		if(left && !right)
		{
			for(int a = 0; a < units.length; a++)
			{
				if(squares[a].contains(p))
					selected = a;
			}


		}

	}



}